Pearson korelasyon katsayısı
iki değerin standart sapmanısının ve kovaryansın arasındaki orandır, kovaryansın normalize edilmiş ölçüsüdür.
pearsonun korelasyon katsayısı populasyona uygulandığında genellikle yunan harfi (rho) ile gösterilir ve populasyon korelasyon katsayısı veya populasyon pearson korelasyon katsayısı olarak refere edilir

Sun Dec 18 2022 09:41:25 GMT+0300 (GMT+03:00)
https://en.wikipedia.org/wiki/Pearson_correlation_coefficient